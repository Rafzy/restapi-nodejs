'use strict'

const response = require('./response')
const connection = require('./dbConnection')

exports.index = (req, res) => {
  response.success('Application RestApi was successfully work', res)
}

// Menampilkan semua data (Read Data)
exports.readData = (req, res) => {
  connection.query('SELECT * FROM mahasiswa', (error, rows, fields) => {
    if (error) {
      console.log(error)
    } else {
      response.success(rows, res)
    }
  })
}

// Menampilkan semua data berdasarkan id
exports.readDataId = (req, res) => {
  const idMahasiswa = req.params.id
  connection.query(`SELECT * FROM mahasiswa WHERE idMahasiswa=${idMahasiswa}`, (error, rows, fields) => {
    if (error) {
      console.log(error)
    } else {
      response.success(rows, res)
    }
  })
}

// Menambahkan Data (Create Data) Menggunakan metode POST
exports.createData = (req, res) => {
  const nim = req.body.nim
  const nama = req.body.nama
  const jurusan = req.body.jurusan

  connection.query(`INSERT INTO mahasiswa (nim, nama, jurusan) VALUES(${nim}, '${nama}', '${jurusan}')`, (error, rows, fields) => {
    if (error) {
      console.log(error)
    } else {
      response.success('Success Create Data', res)
    }
  })
}

// Mengubah Data Berdasarkan id dengan metode PUT
exports.updateData = (req, res) => {
  const idMahasisa = req.params.id
  const nim = req.body.nim
  const nama = req.body.nama
  const jurusan = req.body.jurusan

  connection.query(`UPDATE mahasiswa SET nim='${nim}', nama='${nama}', jurusan='${jurusan}' WHERE idMahasiswa=${idMahasisa}`, (error, rows, field) => {
    if (error) {
      console.log(error)
    } else {
      response.success('Success Update Data', res)
    }
  })
}

// Menghapus data Berdasarkan id dengan metode DELETE
exports.deleteData = (req, res) => {
  const idMahasiswa = req.params.id

  connection.query(`DELETE FROM mahasiswa WHERE idMahasiswa=${idMahasiswa}`, (error, rows, fields) => {
    if (error) {
      console.log(error)
    } else {
      response.success('Success Delete Data', res)
    }
  })
}

// Menampilkan data matakuliah group
exports.dataMatakuliah = (req, res) => {
  connection.query('SELECT mahasiswa.idMahasiswa, mahasiswa.nim, mahasiswa.nama, mahasiswa.jurusan, matakuliah.matakuliah, matakuliah.sks FROM krs JOIN matakuliah JOIN mahasiswa WHERE krs.idMatakuliah = matakuliah.idMatakuliah AND krs.idMahasiswa = mahasiswa.idMahasiswa ORDER BY mahasiswa.idMahasiswa', (error, rows, fields) => {
    if (error) {
      console.log(error)
    } else {
      response.dataNested(rows, res)
    }
  })
}
