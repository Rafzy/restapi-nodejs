'use strict'

module.exports = (app) => {
  const controller = require('./controller')

  app.route('/').get(controller.index)
  app.route('/data').get(controller.readData)
  app.route('/data/:id').get(controller.readDataId)
  app.route('/create').post(controller.createData)
  app.route('/update/:id').put(controller.updateData)
  app.route('/delete/:id').delete(controller.deleteData)
  app.route('/dataNested').get(controller.dataMatakuliah)
}
