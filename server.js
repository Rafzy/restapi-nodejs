const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const morgan = require('morgan')

// Parse data application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// Logger
app.use(morgan('[:method] :remote-user [:date[web]] - [statusCode: :status] - [url: :url] - [speedResponse: :response-time]'))

// Memanggil router
const router = require('./routes')
router(app)

// Mendaftarkan router dari middleware
app.use('/auth', require('./middleware/router'))

app.listen(3000, () => {
  console.log('Server was running on http://localhost:3000')
})
