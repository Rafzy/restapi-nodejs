'use strict'

exports.success = (values, res) => {
  const data = {
    status: 200,
    values: values
  }

  res.json(data)
  res.end()
}

exports.danger = (values, res) => {
  const data = {
    status: 400,
    message: values
  }
  res.json(data)
  res.end()
}

// Response untuk nested matakuliah
exports.dataNested = (values, res) => {
  // Melakukan akumulasi
  const result = values.reduce((dataResult, item) => {
    // Tentukan key groupnya (GROP BY)
    if (dataResult[item.nama]) {
      // Buat Variable group nama mahasiswa
      const group = dataResult[item.nama]

      // Cek jika isi array adalah mata kuliah
      if (Array.isArray(group.matakuliah)) {
        // Tambahkan value ke dalam group matakuliah
        group.matakuliah.push(item.matakuliah)
      } else {
        group.matakuliah = [group.matakuliah, item.matakuliah]
      }
    } else {
      dataResult[item.nama] = item
    }
    return dataResult
  }, {})

  const data = {
    status: 200,
    values: result
  }

  res.json(data)
  res.end()
}
