const mysql = require('mysql')
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'db_restapi'
})

// Connect MySQL
connection.connect((error) => {
  if (error) throw error
  console.log('MySQL berhasil terkoneksi')
})

module.exports = connection
