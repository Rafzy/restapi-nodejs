const md5 = require('md5')
const ip = require('ip')
const jwt = require('jsonwebtoken')
const connection = require('../dbConnection')
const response = require('../response')
const config = require('../config/token')

// Controller registrasi akun
exports.registerAccount = (req, res) => {
  const dataAccount = {
    username: req.body.username,
    email: req.body.email,
    password: md5(req.body.password), // Kita berikan md5 untuk mengenkripsi passwordnya
    role: req.body.role,
    tanggalDaftar: new Date()
  }

  const querySQL = `SELECT email FROM user WHERE email='${dataAccount.email}'` // Cek apakah email sudah terdaftar pada db atau belum

  connection.query(querySQL, (error, rows, fields) => {
    if (error) {
      console.log(error)
    } else {
      if (rows.length === 0) {
        const secondQuerySql = `INSERT INTO user (username, email, password, role, tanggalDaftar) VALUES('${dataAccount.username}', '${dataAccount.email}', '${dataAccount.password}', ${dataAccount.role}, '${dataAccount.tanggalDaftar}')`
        connection.query(secondQuerySql, (error, rows, fields) => {
          if (error) {
            console.log(error)
          } else {
            response.success('Success Create new Account!', res)
          }
        })
      } else {
        response.danger('Error create new account! Email has ready registered', res)
      }
    }
  })
}

// Controller Untuk Login
exports.loginAccount = (req, res) => {
  const dataAccount = {
    email: req.body.email,
    password: req.body.password
  }

  const querySQL = `SELECT * FROM user WHERE password='${md5(dataAccount.password)}' AND email='${dataAccount.email}'`
  connection.query(querySQL, (error, rows, fields) => {
    if (error) {
      console.log(error)
    } else {
      if (rows.length === 1) {
        const token = jwt.sign({ rows }, config.secretToken, {
          expiresIn: 1440 // Token ini akan berlaku hanya dalam 1440second
        })
        const id = rows[0].idUser
        const Account = {
          idUser: id,
          accessToken: token,
          ipAddress: ip.address()
        }

        const secondQuerySql = `INSERT INTO akses_token (idUser, accessToken, ipAddress) VALUES (${Account.idUser}, '${Account.accessToken}', '${Account.ipAddress}')`

        connection.query(secondQuerySql, (error, rows, fields) => {
          if (error) {
            console.log(error)
          } else {
            res.json({
              success: true,
              message: 'Successfully generate token JWT!',
              token: token,
              currUser: Account.idUser
            })
          }
        })
      } else {
        res.json({ Error: true, message: 'Email atau Password salah!' })
      }
    }
  })
}
