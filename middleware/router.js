const express = require('express')
const auth = require('./auth')
const router = express.Router()

// Mendaftarkan router registrasi
router.post('/api/v1/register', auth.registerAccount)
router.post('/api/v1/login', auth.loginAccount)

module.exports = router
